import os
import csv
from sklearn.decomposition import NMF
import numpy as np
from scipy import sparse
import sys

os.chdir("/home/nestor/source/rcode/textMining/csvFiles")
data = np.genfromtxt(str(sys.argv[1]),delimiter=",")
sparseTrain = sparse.csr_matrix((data[:,2],(data[:,0]-1,data[:,1]-1)),shape=(840,2303))
model = NMF(n_components = int(sys.argv[2]),sparseness = 'data',init='random',random_state=int(sys.argv[3]))
fit = model.fit(sparseTrain)
H = fit.components_ # k x n
#W = fit.fit_transform(sparseTrain) #m x k

# sheet = open("/home/nestor/source/rcode/textMining/csvFiles/coef.csv","w")
# writer = csv.writer(sheet)
# for i in range(H.shape[0]):
# 	writer.writerows(H[i,:])
# sheet.close()
np.savetxt('coef_'+str(sys.argv[2])+'_'+str(sys.argv[4])+'.txt',H)

