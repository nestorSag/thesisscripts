setwd("/home/nestor/source/rcode/textMining/Robjects/")

#LDA
library(doParallel)
library(foreach)
library(topicmodels)
load("train_dtm.RData")
load("test_dtm.RData")
cl = makeCluster(detectCores())
registerDoParallel(cl)
t=Sys.time()
ldaList = foreach(i = 2:30, .packages=c("topicmodels")) %dopar% {LDA(train_dtm,k=i)}
print(Sys.time()-t)
save(ldaList,file="ldaList.RData")
stopCluster(cl)

#find optimal number of topics according to stability score
library(doParallel)
library(foreach)
wordsByTopicNMF = function(A,words,w=20){
  k = nrow(A)
  M = matrix(0,w,k)
  for(i in 1:k){
    M[,i]=words[sort(A[i,],decreasing=TRUE,index.return=TRUE)$ix[1:w]]
  }
  return(M)
}
AJ = function(wd1,wd2){
  t = length(wd1)
  s=0
  for(i in 1:t){
    s = s + length(intersect(wd1,wd2))/length(union(wd1,wd2))
  }
  return(s/t)
}
AJMatrix = function(df0,df){
  DF0 = as.matrix(df0)
  DF = as.matrix(df)
  k = ncol(DF)
  M = matrix(0,k,k)
  for(i in 1:k){
    for(j in 1:k){
      M[i,j] = AJ(DF0[,i],DF[,j])
    }
  }
  return(M)
}
agree = function(M,eps=1e-7){
  M[which(M==0,arr.ind=TRUE)]=eps #avoid NA values in matching (a zero weighted edge means no edge in igraph)
  library(igraph)
  k=ncol(M)
  idx = which(M>=0,arr.ind=TRUE)
  idx[,1]=paste("row_",idx[,1],sep="")
  idx[,2]=paste("col_",idx[,2],sep="")
  edgelist = cbind(idx,matrix(M,ncol=1,nrow=k*k))
  g=graph.edgelist(edgelist[,c(1,2)],directed=FALSE)
  E(g)$weight=as.numeric(edgelist[,3])
  V(g)$type=as.logical(substr(V(g)$name,1,3)=="row")
  matching = max_bipartite_match(g)$matching
  matching[is.na(matching)]
  matching=matching[grepl("row",matching)]
  matches = matrix(0,k,2)
  matches[,1]=gsub("(row|col)_","",matching)
  matches[,2]=gsub("(row|col)_","",names(matching))
  matches = apply(matches,2,as.numeric)
  return(sum(M[matches])/k)
}
stabilityScore = function(k,tau = 50,sampleProportion=0.6,words){
  score=rep(0,tau)
  load("train_dtm.RData")
  load("ldaList.RData")
  H = posterior(ldaList[[k-1]],train_dtm)$terms
  ref = wordsByTopicNMF(H,words=words)
  n=dim(train_dtm)[1]
  rm(list=c("ldaList","H"))
  cl = makeCluster(3)
  registerDoParallel(cl)
  score = foreach(i = 1:tau,.packages=c("topicmodels","slam","igraph"),
          .verbose = TRUE,
          .export = c("agree","AJ","AJMatrix","wordsByTopicNMF")) %dopar% {
    idx=sample(1:n,floor(n*sampleProportion))
    sample = as.simple_triplet_matrix(as.matrix(train_dtm[idx,]))
    H = posterior(LDA(sample,k),sample)$terms
    agree(AJMatrix(ref,wordsByTopicNMF(H,words=words)))
  }
  stopCluster(cl)
  return(score)
}

scoreListLDA = list()
load("traindf.RData")
wds=names(train.df)
rm(list=c("train.df"))
for(i in 2:12){
  scoreListLDA[[i]] = stabilityScore(i,words=wds)
}
save(scoreListLDA,file="scoreListLDA.RData")

#number of topics validation
library(ldatuning)
library(doParallel)
library(foreach)
load("train_dtm.RData")
ldaValidation <- FindTopicsNumber(
  train_dtm,
  topics = seq(from = 5, to = 100, by = 5),
  metrics = c("Griffiths2004", "CaoJuan2009", "Arun2010", "Deveaud2014"),
  method = "Gibbs",
  control = list(seed = 77),
  mc.cores = 3L,
  verbose = TRUE
)