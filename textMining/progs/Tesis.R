setwd("/home/nestor/source/rcode/Text Mining")
#This code was used for my thesis work, which is about predicting the decisions
#of the US supreme court using text mining, and specially regular expressions.
# lets se what we can do.
library(stringr)

#downlaod the transcripts webpage
url="http://www.supremecourt.gov/oral_arguments/argument_transcripts.aspx"
download.file(url,destfile="rawhtml.txt")
html=readChar("rawhtml.txt",nchar=1e7)

#extract the file names from the downloaded webpage

ptrn="argument_transcripts/[-0-9a-zA-Z_]+[.]pdf"
files=vector()
flag=0
files=str_extract_all(html,ptrn)
files=unlist(files)

#now, the pdf files will be downloaded and transformed in text files

raw_text=vector()
n=length(files)
for(i in 1:n){
  #WARNING: although this program tries to read all the transcripts in the US Supreme Court website,
  # some of the oldest documents don't have copy permission (I don't know why), so at the time I wrote 
  #the code, only 967 files could be read.
  
  
#*********************************************************************************
#this chunk of code is an adaptation of the code that can be found in 
#https://github.com/gimoya/theBioBucket-Archives/blob/master/R/txtmining_pdf.R
#for reading the text in pdf files

file_url=paste(url,files[i],sep="")
dest = tempfile(fileext = ".pdf")
download.file(file_url, dest, mode = "wb",quiet=TRUE)
exe = "C:\\Users\\nestor\\Documents\\xpdfbin-win-3.04\\bin32\\pdftotext.exe"
system(paste("\"", exe, "\" \"", dest, "\"", sep = ""), wait = T)

#*********************************************************************************


#read the text and put it into a vector entry 
filetxt <- sub(".pdf", ".txt", dest)

#read the document as a string, and clean it
docu<-readChar(filetxt,nchar=1e7)
raw_text=c(raw_text,docu)
#delete the files created outside R
unlink(filetxt)
filepdf=sub(".txt", ".pdf", dest)
unlink(filepdf)
}
write.csv(raw_text,"rawtext.csv")

#inicialize the necessary variables and vectors


library(stringr)
w=0
url="http://www.supremecourt.gov/oral_arguments/" 
ptrn_begin="((P R O C E E D I N G S?|PROCEEDINGS?).*?(\\(|\\[|\\{): *(a|p)[.]m[.](\\)|\\]|\\}) *|(\\(|\\[|\\{): *(a|p)[.]m[.](\\)|\\]|\\}) *CHIEF)" #matches the beginning of the argument
ptrn_end="((?i) *case *is *(now)? *submitted|the *case *in *the *above-entitled *matter *was *submitted)" #matches the end of the argument
ptrn_num="No. [0-9]{2}-?[0-9]+" #regex that matches the case number
crp=vector() #text corpus
case=vector() #vector with each cases docket number
type=vector() # particular vs particular or government vs particular
jst_args=vector() #string vector with everything the justices said
pet_args=vector() #string vector with everything the petitioners representants said
rsp_args=vector() #string vector with everything the respondents representants said
unk_args=vector() 
int_pet=vector() #number of interventions on behalf of petitioners
int_rsp=vector() #number of interventions on behalf of respondents
speakers=list() #list that contains all the speakers by case
side=list() #list that contains the side (pet, rsp, jst) of every speaker by case
metadatos=vector() #contains information about the participants of each case
raw_text=read.csv("rawtext.csv")
raw_text=raw_text[,2]
n=length(raw_text)
Pets=list()
Rsps=list()
for(i in 1:n){
  docu<-raw_text[i]
  #extract the case number of each file and do very basic cleaning
  docu<-gsub("\\r","",docu)
  docu<-gsub("\\n","",docu)
  docu<-gsub("\\f","",docu)
  docu<-gsub(" +"," ",docu)
  docu<-gsub("ESQ.","",docu)
  docu<-gsub(",","",docu)
  docu<-gsub("(JR|MR|MS).","",docu) #to avoid some troubles when findings parties names
  docu<-gsub("[a-zA-Z]+ [a-zA-Z]+ Company(Official|Washington)","",docu)
  docu<-gsub("(?i)[a-z]*washington[a-z]*","",docu)
  docu<-gsub("(?i)[a-z]*alderson[a-z]*","",docu)
  
  
  num=str_extract(docu,ptrn_num)
  docu<-gsub("[0-9]","",docu)
  
  #discard content that is not part of the arguments
  ending=str_locate(docu,ptrn_end)[1,1]-1
  begin=str_locate(docu,ptrn_begin)[1,2]+1
  metadata=substr(docu,1,begin)
  docu=substr(docu,begin,ending)
  

  
  #extract the argument of each speakers 
  argm=strsplit(docu,"(CHIEF )?(JUSTICE )?[A-Z']{2,}: ")
  spkrs=str_extract_all(docu,"(CHIEF )?(JUSTICE )?[A-Z]{2,}:")
  argm=unlist(argm) #chuncks of arguments
  argm=argm[-1]
  spkrs=unlist(spkrs) #vector of cronologically ordered speakers (repeated names)
  unq_spk=unique(spkrs) #name of all the speakers (no repeated names)
  
  #separate the different arguments (by speaker)
  n_spk=length(unique(spkrs))
  raw_arg=c(rep(0,n_spk))
  
  for(j in 1:n_spk){
    speaker=unq_spk[j]
    ind=which(spkrs==speaker)
    args=argm[ind]
    raw_arg[j]=paste(args,collapse="")
  }
  
  
  #we need to know what was said in behalf of the petitioners/respondents
  cont=1:n_spk
  status1=c(rep("unk",n_spk)) #vector that will contain the status (unk, jst, pet, rsp) of every speaker
  status1[cont[grepl("(QUESTION:|JUSTICE)", unq_spk)]]="jst"
  
  
  #list of people who are not justices
  labels=1:n_spk
  not_jst=unq_spk[!grepl("(QUESTION:|JUSTICE)", unq_spk)]
  labels=labels[!grepl("(QUESTION:|JUSTICE)", unq_spk)]
  n=length(not_jst)
  status=c(rep(0,n))
  
  for(k in 1:n){ #this loop clasifies the speakers by respondant, petitioners or unknown
    name=paste("(?i)",not_jst[k],"?.*?(petitioners?|respondents?|plaintiffs?|defendants?)",sep="")
    if(grepl(name,metadata)){
      aux=str_extract(metadata,name)
      if(grepl("(?i)(petitioners?|plaintiffs?)",aux)) status[k]="pet"
      else status[k]="rsp"
    }
  }
  
  pet_spk=which(status=="pet")
  status1[labels[pet_spk]]="pet"
  rsp_spk=which(status=="rsp")
  status1[labels[rsp_spk]]="rsp"
  unk_spk=which(status=="unknown")
  
  # number of interventions on behalf of petitioners/respondents
  pets=unq_spk[status1=="pet"] #names of petitioners (speakers)
  rsps=unq_spk[status1=="rsp"] #names of respondents (speakers)
  if(length(pets)>0)Pets[[n]]=pets
  if(length(rsps)>0)Rsps[[n]]=rsps
  no_pets=length(pets) #number of petitioners (speakers)
  no_rsps=length(rsps) #number of respondents (speakers)
  s=0
  for (q in 1:no_pets) {s=s+sum(spkrs==pets[q])}
  t=0
  for (q in 1:no_rsps) {t=t+sum(spkrs==rsps[q])}

  
  
  #store all the relevant variables only if there were no errors reading the file (by the time I wrote this code, 
  #there were 26 files that the program couldn't read well, mainly because of subtle spelling error in the
  #transcriptions, and modifying the code to capture that files was so complicated that it didn't really worth it
  #or the modification could end up capturing wrong pieces of text in all the other files)
  
  #to know wether a file is correcly or wrongly captured, I just see the number of interventions
  #of the petitioners: if its 0 or NA something went wrong; if not, most likely all is ok
  if(!is.na(s) & s>0 & !is.na(t) & t>0){
    w=w+1
    int_rsp=c(int_rsp,t)
    int_pet=c(int_pet,s)
    crp=c(crp,docu)
    side[[w]]=status1
    speakers[[w]]=unq_spk
    pet_args=c(pet_args,paste(raw_arg[labels[pet_spk]],collapse="")) #arguments on behalf of petitioner
    rsp_args=c(rsp_args,paste(raw_arg[labels[rsp_spk]],collapse="")) #arguments on behalf of respondent
    unk_args=c(unk_args,paste(raw_arg[labels[unk_spk]],collapse="")) #arguments of unknown origin
    metadatos=c(metadatos,metadata)
    #what the justices said
    jst_args=c(jst_args,paste(raw_arg[grepl("(QUESTION:|JUSTICE)", unq_spk)],collapse=""))
    case=c(case,num)
  }
}

#once that we have all the data well organized, we get rid of all the (now) useless auxiliar variables
save(crp,int_pet,int_rsp,metadatos,pet_args,rsp_args,unk_args,jst_args,case,rsps,pets,no_rsps,
     no_pets,file="metadatosCrp.RData")
save(crp,file="crp.RData")
rm(list=ls(all=TRUE))
load("crp.RData")
#Now that all the data has been downloaded, organized and cleaned, we proceed to analyze the documents
#first, we apply some clustering techniques to see how many different "topics" are there
#first, get rid of the speakers names in the raw texts
#crp1 original corpus
crp<-gsub("(CHIEF )?(JUSTICE )?[A-Z']{2,}: ?","",crp)
#divide the data set in train and test sets (80-20)
n=length(crp)
ntrain=840
set.seed(117349)
index=sample(n,ntrain)
save(index,file="index.RData")
train=crp[index]
test=crp[-index]
#we transform and clean all our text data to do the traditional BOW analysis
library(tm)
library(SnowballC)
train.bow<-VectorSource(train)
train.bow=VCorpus(train.bow)
train.bow=tm_map(train.bow,content_transformer(tolower))
train.bow=tm_map(train.bow,removePunctuation)
train.bow=tm_map(train.bow,removeWords,stopwords("english"))
train.bow=tm_map(train.bow,stemDocument)
train.bow=tm_map(train.bow,stripWhitespace) #clean train set
train.dtm=DocumentTermMatrix(train.bow) #document-term matrix of the train set
# delete uncommon terms (if a term is very uncommon, this is, it appeas in less than, say, 5% of the 
#documents, it is probably a typing error,a proper name, or some irrelevant word for the clustering procedure)
train.dtm=removeSparseTerms(train.dtm,0.95) #document-term matrix of the train set
save(train.dtm,file="traindtm.RData")

tfidf.train=weightSMART(train.dtm,spec= "ntn")
tfidf.df=as.data.frame(inspect(tfidf.train))
mtfidf=as.numeric(sapply(tfidf.df,mean))
train.df=as.data.frame(inspect(train.dtm))
train.df=train.df[,as.logical(mtfidf>=0.4)]

#extract from the test set the features observed in the train set
train.names=names(train.df) #train set's observed features

test.bow<-VectorSource(test)
test.bow=VCorpus(test.bow)
test.bow=tm_map(test.bow,content_transformer(tolower))
test.bow=tm_map(test.bow,removePunctuation)
test.bow=tm_map(test.bow,removeWords,stopwords("english"))
test.bow=tm_map(test.bow,stemDocument)
test.bow=tm_map(test.bow,stripWhitespace) #clean test set
test.dtm=DocumentTermMatrix(test.bow) #document-term matrix of the test set
test.dtm=removeSparseTerms(test.dtm,0.95) #document-term matrix of the test set
test.df=as.data.frame(inspect(test.dtm))
test.names=names(test.df)

test.m=c()
for(i in 1:length(train.names)){
  if(is.element(train.names[i],test.names)){
    index=which(test.names==train.names[i])
    test.m=cbind(test.m,test.df[,index])
  }
  else{
    test.m=cbind(test.m,matrix(0,length(test),1))
  }
}
test.m=as.data.frame(test.m)
names(test.m)=train.names
test.df=test.m #this variable contains the matrix that contains the features corresponding to the
#test set


#Latent Semantic Indexing
library(lsa)
library(Matrix)
k=500 #number of semantic topics
train.lsa=lsa(t(train.df),dim=k)
U=train.lsa[[1]]
S=as.matrix(Diagonal(n=k,train.lsa[[3]]))
V=train.lsa[[2]]
save(U,file="U.RData")
save(S,file="S.RData")
save(V,file="V.RData")

### hierarchical clustering with LSI 
load("V.RData")
V=as.data.frame(V)
hc=hclusterpar(V,method = "euclidean", link = "ward", nbproc = 1)
plot(hc,labels=FALSE)
labels_lsi=predict.hclust(hc,V,V,4)
save(labels_lsi,file="labels_lsi.RData") #save labels of training set
save(hc,file="hc.RData")


#NMF (Nonnegative Matrix Factorization clustering)
library(NMF)
train.nmf<-nmf(as.matrix(train.df),4,method="lee")
W<-basis(train.nmf)
H<-coef(train.nmf)
save(H,file="H.RData")
save(W,file="W.RData")



#asign the cluster label by taking the maximum axis value
max.label=vector()
for(i in 1:nrow(train.df)){
  max.label=c(max.label,which(W[i,]==max(W[i,])))
}
max.label<-cbind(1:nrow(train.df),max.label)
max.label<-as.data.frame(max.label)
names(max.label)<-c("Docs","labels_nmf")
labels_nmf=max.label$labels_nmf
save(labels_nmf,file="labels_nmf.RData")

### hierarchical clustering with NMF 
load("H.RData")
load("W.RData")
W=as.data.frame(W)
hc=hclusterpar(W,method = "euclidean", link = "ward", nbproc = 1)
plot(hc,labels=FALSE,hang=-1)
labels_lsi=predict.hclust(hc,V,V,4)
save(labels_lsi,file="labels_lsi.RData") #save labels of training set
save(hc,file="hc.RData")


#delete the variables that we are not going to use again
save(train.df,file="traindf.RData")
save(train.dtm,file="traindtm.RData")
save(test.df,file="testdf.RData")
save(train,file="train.RData")
save(test,file="test.RData")
save(V,file="V.RData")
save(mtfidf,file="mtfidf.RData")
rm(list=ls(all=TRUE))



###### FINDING REPRESENTATIVE WORDS FOR EACH CLUSTER  #######

#now, we have to find the most representative words of each cluster. we can't simply use the most
#frequent words of each cluster, because that words are common in every document (court, client, right,...)
#instead, we measure the discriminative power of each word using mutual information.
#separate the documents by cluster


#------------------------------------------------------------------------------------------------------
#This chunk of code finds the most representative words for each cluster using the puntial MI score 
#we make a mutual information matrix; in every column, it will have one word, and the MI score for every cluster
P=vector()
for(i in 1:nclust){
  P=c(P,nrow(train.clust[[i]]))
}
P=P/nrow(train.df)
MI=matrix(0,nclust,ncol(train.df)) #matrix of mutual information for each word and cluster
num=1:nrow(train.df)
aux=as.matrix(train.df)
for(w in 1:ncol(train.df)){
  p_i_w=vector()
  word_presence=num[aux[,w]>0] #documents in which the word is present
  for(i in 1:nclust){
    p_i_w=c(p_i_w,sum(ordered.labels[word_presence]==i)) #probability of cluster i given word w (almost)
  }
  p_i_w=p_i_w/length(word_presence) #probability of cluster i given word w
  MI[1:nclust,w]=p_i_w/P #MI scores for word w
}

#now, we need to find the most representative words for each cluster.
cluster=vector()
value=vector()
for(i in 1:ncol(train.df)){
  cluster=c(cluster,as.numeric(which(MI[,i]==max(MI[,i]))[1])) #we identify the cluster for which each word
                                                              # has the higher MI score
  value=c(value,max(MI[,i]))
}
#then, we make a matrix where the k-th column has the most important words of the k-th cluster

#but first, we need to make a dictionary of the corpus (to reconstruct the stemmed words)
dict=paste(train,collapse="")
dict=gsub("[.:,;?/]","",dict )
dict=gsub(" +"," ",dict )
dict=unlist(strsplit(dict," "))
dict=tolower(dict)

clust.word=matrix(0,20,nclust)
stemmed=matrix(0,20,nclust)
clust.names=vector()
for(i in 1:nclust){
  MIk=value[cluster==i]
  prueba=colnames(train.df)[cluster==i] #words that are asociated to cluster i acording to MI
  kwords=prueba[sort(MIk,decreasing=TRUE,index.return=TRUE)$ix][1:20] #find the most "important" terms in cluster k
  stemmed[,i]=kwords
  kwords=stemCompletion(kwords,dict,type="prevalent") #reconstruct the stemmed words
  clust.word[,i]=kwords
  clust.names=c(clust.names,paste("cluster ",i,sep=""))
}
clust.word=as.data.frame(clust.word) #clust.words contains the most important words for every cluster
                                     #acording to the MI score
names(clust.word)=clust.names

save(stemmed,file="stemm_cprob_nmfmax.RData")
save(clust.word,file="complete_cprob_nmfmax.RData")
rm(list=ls(all=TRUE))
#---------------------------------------------------------------------------------------------------------
load("repwords.RData")

#merge the data we have with the Supreme Court Database, matching each case by its docket number
#and writing as new variables each one of the justices' votes 
load("case.RData")
load("index.RData")
casenum=gsub("(No. |-)","",case)
load("scd.RData")
scd=scdb3
scd$docket=gsub("[^0-9]+","",as.character(scd$docket))
scd=scd[nchar(scd$docket)>0,]
scd.reordered=scd[1:length(casenum),]
jnames= unique(scd$justiceName)
justices.votes=matrix(0,length(casenum),length(jnames))
for(i in 1:length(casenum)){
  for(j in 1:nrow(scd)){
    flag=grepl(paste("^",as.character(scd$docket[j]),sep=""),casenum[i])
    if(flag){
      scd.reordered[i,]=scd[j,]
      names=scd$justiceName[j:(j+8)]
      votes=scd$vote[j:(j+8)]
      for(l in 1:9){
        k=which(jnames==names[l])
        justices.votes[i,k]=votes[l]
      }
      j=nrow(scd)
    }
  }
}
scd.reordered=as.data.frame(scd.reordered)
names(scd.reordered)=names(scd)
justices.votes=as.data.frame(justices.votes)
names(justices.votes)=jnames
#add the single justices' votes as variables 

scd.train=scd.reordered[index,]
scd.test=scd.reordered[-index,]
save(justices.votes,file="votes.RData")
save(scd.train,file="scdtrain.RData")
save(scd.test,file="scdtest.RData")
save(justices.votes,file="justvotes.RData")
y.train=scd.train$partyWinning
y.test=scd.test$partyWinning
save(y.train,y.test,file="response.RData")

#this vector shows the rows that have not a match (all the entries that have FALSE)
#that's because the docket number of some cases is different in the transcript and in the database
#for that cases, we construct a regex that matches them with their database entry
v=vector()
for(i in 1:length(case.train)){
  v=c(v,grepl(scd.reordered$docket[i],case.train[i]))
}
count=1:length(v)
count=count[v==FALSE]

#-------------------------------------------------
#label test data according to hierarchical clustering in LSI space
library(class)
load("traindf.RData")
load("V.RData")
load("u.RData")
load("S.RData")
#put the V rows in the same order as y.train
n=nrow(train.df)


#project test set onto LSI space
Sinv=matrix(0,nrow(S),ncol(S)) #S is diagonal so its inverse is easy to compute
for(i in 1:nrow(S)){
  Sinv[i,i]=1/S[i,i]
}
test.lsi=Sinv%*%t(U)%*%t(as.matrix(test.df)) #test set projected onto lsi space: d_hat=Sinv*t(U)*d
test.lsi=t(test.lsi)
save(test.lsi,file="testlsi.RData")

load("testlsi.RData")
load("hc.RData")
test.lsi=as.data.frame(test.lsi)
test_lsi_labels=predict.hclust(hc,test.lsi,V,4)
save(test_lsi_labels,file="test_lsi_labels.RData")
#-----------------------------------------------
