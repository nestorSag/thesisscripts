# README #

This repository contains the R and Python scripts that I used for my bachelor thesis, and also some important csv files and results figures.

My bachelor thesis was about using textual information from the hearings' transcripts to make Machine Learning models that could predict the decision direction in U.S. Supreme Court cases. The cases used here were downloaded from the U.S. Supreme Court official website in july 2014, and contains the hearings from the years 2000-2013.

### What is this repository for? ###

The files work independently of each other (always that the required R objects can be loaded*), and each one performs a task specified in the file name, for instance some form of data cleaning or some particular clustering method. 

The files are intended to run in batches (delimited by comments in the code) that perform a specific action rather than the whole file at once, although in theory they should be able to. 
Results replication can be easily achieved running the necessary batches.

*the R objects used weren't uploaded to this repository, since they are too heavy.
For that reason, to replicate results from the last stages of my thesis, it would be necessary to run first the script 'Tesis.R' which creates the most important R objects that are used subsequently, from the 
lines 60 onwards. It needs the file 'rawtext.csv' that is in the 'csvFIles' folder. This file
contains all the textual information used in this work.

* I started this project way before getting into software development and when I didn't know a lot about R, so organization and elegance are not this project's strenght. Sorry about that! :)
### Who do I talk to? ###

nestor.sag@gmail.com